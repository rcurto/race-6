(function (angular) {
    'use strict';

    angular.module('MetronicApp').
        controller('raceFormController', raceFormController);

    raceFormController.$inject = ['$scope', 'cursesFactory', '$location', '$state', 'circuitsFactory','countryFactory','$filter'];
    function raceFormController($scope, cursesFactory, $location, $state, circuitsFactory, countryFactory, $filter) {

        $scope.$parent.initPage = init; // start point

        $scope.env = {
            post_id: $state.params.id,
            loading: true,
            saving: false,
            model: {},
            error: null,
            circuits:[],
            country:[]
        };
        var promises = [];
        $scope.user = null;

        function init() {
            var user = $scope.$parent.user;
            $scope.user = user;

            var countryPromise = countryFactory.getAll(null, false).then(function (posts) {
                $scope.env.country = posts;
            });
            promises.push(countryPromise);


            var joinedPromise = user.getJoinCoursesIds().then(function (ids) {
                $scope.env.joins = ids;
            });
            promises.push(joinedPromise);


            var circuitsPromise = circuitsFactory.getAll().then(function (posts) {
                $scope.env.circuits = posts;
            });
            promises.push(circuitsPromise);


            if ($scope.env.post_id != undefined) {
                var racePromise = cursesFactory.getById($scope.env.post_id).then(function (post) {
                    $scope.env.model = post.attributes;
                    $scope.env.post = post;

                });
                promises.push(racePromise);
            }

            Parse.Promise.when(promises).then(function () {
                pageLoaded();
            });
        }

        function pageLoaded() {
            $scope.env.loading = false;
            $scope.$apply();
        }

        $scope.save = function (data) {
            $scope.env.saving = true;
            if ($scope.env.post_id != undefined) {
                $scope.env.post.update(data, function (result) {
                    $scope.env.saving = false;
                    if (result.success === false) {
                        $scope.env.error = result.error;
                        $scope.$apply();
                    } else {
                        alertify.success( $filter('translate')('Race changed'));
                        $location.path('/races');
                        $scope.$apply();
                    }
                });
            } else {
                cursesFactory.store(data, function (result) {
                    $scope.env.saving = false;
                    if (result.success === false) {
                        $scope.env.error = result.error;
                        $scope.$apply();
                    } else {
                        alertify.success( $filter('translate')('Race created'));

                        $scope.$apply(function () {
                            $location.path('/races');
                        });
                    }
                })
            }


        }

    }
})(angular);