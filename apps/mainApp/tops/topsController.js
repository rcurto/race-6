(function () {

    'use strict';

    angular
        .module('MetronicApp')
        .controller('topsController', topsController);

    topsController.$inject = ['$scope', 'userFactory', 'cursesFactory', '$filter'];

    function topsController($scope, userFactory, cursesFactory, $filter) {
        $scope.$parent.initPage = init; // start point

        $scope.env = {
            loading: true,
            user: null,
            race: null,
            nextRace: null,
            topRaces: [],
            topRacesWithYouVote: [],
            topRecords: [
                {field: 'VO2max', title: $filter('translate')('VO2max'), sorting: 'desc', show: false},
                {field: 'hrMax', title: $filter('translate')('HRmax'), sorting: 'desc', show: false},
                {field: 'hrRest', title: $filter('translate')('HRrest'), sorting: 'asc', show: false},
                {field: 'VT1', title: $filter('translate')('%VT1'), sorting: 'desc', show: false},
                {field: 'VT2', title: $filter('translate')('%VT2'), sorting: 'desc', show: false},
                {field: 'rec60', title: $filter('translate')('Recovery 60" (%)'), sorting: 'asc', show: false},
                {field: 'rec120', title: $filter('translate')('Recovery 120" (%)'), sorting: 'asc', show: false},
                {field: 'test400', title: $filter('translate')('Sweat Rate'), sorting: 'asc', show: false},
                {
                    field: 'test1000',
                    title: $filter('translate')('Record Road Marathon (min)'),
                    sorting: 'asc',
                    show: false
                },
                {
                    field: 'sweat',
                    title: $filter('translate')('Record Road Half-Marathon (min)'),
                    sorting: 'desc',
                    show: false
                },
                {field: 'record42', title: $filter('translate')('Record Road 10K (min)'), sorting: 'asc', show: false},
                {field: 'record21', title: $filter('translate')('Test 400 m (sec)'), sorting: 'asc', show: false},
                {field: 'record10', title: $filter('translate')('Test 1000 m (sec)'), sorting: 'asc', show: false}
            ],
            topRecordsResult:{},
            countShowTopRecords:0,
            topRunningTrailShoes: [],
            topRunningRoadShoes: [],
            topErgogenicAids: [],
            topRunningWatchModel: [],

        };
        var promises = [];
        $scope.user = null;

        function init() {
            var user = $scope.$parent.user;
            $scope.user = user;

            var userRacesPromise = user.getRaces().then(function (userRaces) {
                $scope.env.userRaces = userRaces;
            });
            promises.push(userRacesPromise);

            var topRacesPromise = cursesFactory.topRaces(10).then(function (races) {
                $scope.env.topRaces = races;
            });
            promises.push(topRacesPromise);

            /**
             * Top records
             */
            angular.forEach($scope.env.topRecords, function (rec) {
                if ($scope.user.get(rec.field) != undefined) {
                    $scope.env.countShowTopRecords++;
                    rec.show = true;
                }
            });
            var topRecordsPromise = userFactory.getTopRecords($scope.env.topRecords, user).then(function (result) {
                $scope.env.topRecordsResult = result;
            });
            promises.push(topRecordsPromise);


            var topRunningTrailShoesPromise = userFactory.getTopRunningTrailShoes(function (result) {
                $scope.env.topRunningTrailShoes = result;
            });
            promises.push(topRunningTrailShoesPromise);


            var topErgogenicAidsPromise = userFactory.getTopErgogenicAids().then(function (result) {
                $scope.env.topErgogenicAids = result;
            });
            promises.push(topErgogenicAidsPromise);


            var topRunningRoadShoesPromise = userFactory.getTopRunningRoadShoes().then(function (result) {
                $scope.env.topRunningRoadShoes = result;
            });
            promises.push(topRunningRoadShoesPromise);


            var topRunningWatchModelPromise = userFactory.getTopRunningWatchModel().then(function (result) {
                $scope.env.topRunningWatchModel = result;
            });
            promises.push(topRunningWatchModelPromise);


            Parse.Promise.when(promises).then(function () {

                for (var i in $scope.env.topRaces) {
                    for (var j in  $scope.env.userRaces) {
                        if ($scope.env.userRaces[j].get('nomcursa').id == $scope.env.topRaces[i].getId()) {
                            $scope.env.topRacesWithYouVote.push($scope.env.topRaces[i].getId())
                        }
                    }
                }


                pageLoaded();
            });
        }


        function pageLoaded() {
            $scope.env.loading = false;
            $scope.$apply();
        }

    }

})();