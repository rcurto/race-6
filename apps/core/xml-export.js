(function (angular) {
    'use strict';
    angular.module('core').service('xmlExport', xmlExport);

    // xmlExport.$inject = [];

    function xmlExport() {

        return {
            getXml: getXml,
        };
        function getXml(headers, json) {


            var content = emitXmlHeader(headers);
            content += jsonToSsXml(json);
            content += emitXmlFooter();
            return content;
        }


        function emitXmlHeader(headers) {
            var headerRow = '<ss:Row>\n';
            for (var i in headers) {
                headerRow += '  <ss:Cell>\n';
                headerRow += '    <ss:Data ss:Type="String">';
                headerRow += headers[i] + '</ss:Data>\n';
                headerRow += '  </ss:Cell>\n';
            }
            headerRow += '</ss:Row>\n';
            return '<?xml version="1.0"?>\n' +
                '<ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">\n' +
                '<ss:Worksheet ss:Name="Sheet1">\n' +
                '<ss:Table>\n\n' + headerRow;
        };

        function emitXmlFooter() {
            return '\n</ss:Table>\n' +
                '</ss:Worksheet>\n' +
                '</ss:Workbook>\n';
        };

        function jsonToSsXml(data) {
            var row;
            var col;
            var xml = '';
            for (row = 0; row < data.length; row++) {
                xml += '<ss:Row>\n';
                for (col in data[row]) {
                    xml += '  <ss:Cell>\n';
                    xml += '    <ss:Data ss:Type="String">';
                    xml += (data[row][col] == undefined ? '' : data[row][col]) + '</ss:Data>\n';
                    xml += '  </ss:Cell>\n';
                }
                xml += '</ss:Row>\n';
            }
            return xml;
        }

    }
})(angular);

