(function (angular) {
    'use strict';
    angular.module('core').service('atletaCursaService', atletaCursaService);

    atletaCursaService.$inject = [];
    function atletaCursaService() {


        return function (parseObject) {

            var object = angular.copy(parseObject.attributes);

            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);
            if ( parseObject.get('nomcursa') ){
                object.nomcursa = angular.copy( parseObject.get('nomcursa').attributes);
                object.nomcursa.data = moment( object.nomcursa.data ).utc().format('YYYY-MM-DD');
                object.printData = moment( object.nomcursa.data ).format('DD-MM-YYYY');
                object.sortData = moment( object.nomcursa.data ).format('YYYY-MM-DD');
                object.season = moment( object.nomcursa.data ).format('YYYY');
                object.nomcursa.toData = moment( object.nomcursa.data ).utc().toDate();
                object.shortData = moment( object.nomcursa.data ).format('DD MMM');
                object.isPastRast = !moment().isAfter( moment(object.nomcursa.data,"YYYY-MM-DD") );
            }

            if ( parseObject.get('user') ){
                object.user = angular.copy( parseObject.get('user').attributes);
                object.userName = angular.copy( parseObject.get('user').get('username'));
                object.username = angular.copy( parseObject.get('user').get('username'));
                object.trainingPlan = angular.copy( parseObject.get('user').get('trainingPlan'));
            }

            object.attributes.createdAt = moment(object.parseObject.createdAt).utc().format('DD-MM-YYYY');

            object.get = function (key) {
                return object.attributes[key];
            };
            object.getId = function () {
                return object.parseObject.id;
            };
            object.getCreatedAt = function () {
                return object.parseObject.createdAt;
            };
            object.getCurseData = function () {
                if ( object.get('nomcursa') ){
                    return moment(object.get('nomcursa').get('data')).utc().format('DD-MM-YYYY')
                }
            };

            object.update = function (data, callback) {
                var i;
                for (i in data) {
                    if (i=='parseObject' || data[i]==undefined){
                        continue;
                    }
                    if ( i=='joined' ){
                        continue;
                    }
                    object.parseObject.set(i, data[i]);
                }
                return object.parseObject.save().then(function(){callback()})

            };

            object.saveBeforeCoachComment = function(msg){
                object.parseObject.set('beforeCoachComment', msg);
                object.parseObject.save()
            };

            object.delete = function (callback) {
                this.parseObject.destroy({
                    success: function (myObject) {

                        if (callback)
                        callback({success:true})
                    },
                    error: function (myObject, error) {
                        callback({success:false,error:error})
                    }
                });
            };

            object.checkFeedback = function () {
                if ( this.posabs==undefined ){
                    return true
                }
                if ( this.temps_segons==undefined ){
                    return true
                }
                if ( this.feelings==undefined ){
                    return true
                }
                if ( this.rating==undefined ){
                    return true
                }
                return false;
            };


            return object;

        }
    }
})(angular);

