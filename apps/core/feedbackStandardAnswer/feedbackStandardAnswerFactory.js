(function (angular) {
    'use strict';
    angular.module('core').service('feedbackStandardAnswerFactory', feedbackStandardAnswerFactory);

    feedbackStandardAnswerFactory.$inject = ['feedbackStandardAnswerService'];

    function feedbackStandardAnswerFactory(feedbackStandardAnswerService) {
        return {
            getAllByType: getAllByType
        };


        function getAllByType(type) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("FeedbackStandardAnswer");
            var query = new Parse.Query(parseObject);

            query.equalTo('type',type).find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(feedbackStandardAnswerService(row));
                    });
                    promise.resolve(rows);

                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;

        }


    }
})(angular);

